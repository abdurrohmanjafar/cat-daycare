import { useInView } from "react-intersection-observer";
import styles from "./Activities.module.css";
import Activity from "./Activity";

function Activities() {
  const { ref, inView } = useInView({
    threshold: 0.2,
    triggerOnce: true,
  });

  return (
    <section
      id="activities"
      className={`activities scroll-section ${
        inView ? "section-show" : "section-hidden"
      } ${styles.activities}`}
      ref={ref}
    >
      <h2 className={styles.sectionTitle}>Activities</h2>
      <div className={styles.activitiesItem}>
        <Activity icon="#icon-heart-full" title="Meet & Greet">
          Socialization with the other cat
        </Activity>
        <Activity icon="#icon-trophy" title="Fun & Games">
          Physical & congnitive playtime with toys & activities like hide & seek
        </Activity>
        <Activity icon="#icon-key" title="Skill Enrichment">
          Practicing skills & tricks
        </Activity>
        <Activity icon="#icon-heart" title="Lunch & Treats">
          Break for meal or snack
        </Activity>
        <Activity icon="#icon-lock" title="Rest & Relaxation">
          Naptime with calming music
        </Activity>
        <Activity icon="#icon-global" title="Meet & Greet">
          Sensory stimulation with room full of balls
        </Activity>
        <Activity icon="#icon-map-pin" title="Walk Around">
          Walk around with your lovely cats
        </Activity>
        <Activity icon="#icon-presentation" title="Cat Show">
          Fun contest between the cats
        </Activity>
      </div>
    </section>
  );
}

export default Activities;
