import sprite from "../svg/sprite-1.svg";
import styles from "./Activity.module.css";

function Activity({ icon, title, children }) {
  return (
    <div className={styles.activity}>
      <svg className={styles.activityLogo}>
        <use href={sprite + icon} />
      </svg>
      <p className={styles.activityTitle}>{title}</p>
      <p className={styles.activityText}>{children}</p>
    </div>
  );
}

export default Activity;
