import Navbar from "./Navbar";
import Header from "./Header";
import Facilities from "./Facilities";
import Activities from "./Activities";
import Pricing from "./Pricing";
import Footer from "./Footer";

function App() {
  return (
    <div className="App container">
      <Navbar />
      <Header />
      <Facilities />
      <Activities />
      <Pricing />
      <Footer />
    </div>
  );
}

export default App;
