import { useInView } from "react-intersection-observer";
import styles from "./Facilities.module.css";
import Facility from "./Facility";

function Facilities() {
  const { ref, inView } = useInView({
    threshold: 0.1,
    triggerOnce: true,
  });

  return (
    <section
      id="facilities"
      className={`facilities scroll-section ${
        inView ? "section-show" : "section-hidden"
      } ${styles.container}`}
      ref={ref}
    >
      <h2 className={styles.sectionTitle}>Why Choose Us</h2>
      <Facility imgSrc="public-transport.jpg" title="Good Location">
        Our location is in the middle of Jakarta. Our place can be easily
        accessed using various types of public transportation such as buses and
        MRT.
      </Facility>
      <Facility imgSrc="park-area.jpg" title="Spacious Play and Park Area">
        We have a large play area, so you don't need to worry about your pet
        getting bored. We have a large parking area so you won't have any
        trouble bringing your vehicle.
      </Facility>
      <Facility imgSrc="certificate.jpg" title="Certified Pet Sitter">
        All of our staff are certified pet sitters. so we can provide the best
        service
      </Facility>
      <Facility imgSrc="vet.jpg" title="Veterinarian Service">
        We have a veterinarian on duty, so you don't need to worry about your
        pet's health
      </Facility>
    </section>
  );
}

export default Facilities;
