import styles from "./Facility.module.css";

function Facility({ imgSrc, children, title }) {
  return (
    <div className={styles.facility}>
      <figure className={styles.facilityImageContainer}>
        <img
          src={require(`../img/${imgSrc}`)}
          alt="public transport"
          className={styles.facilityImage}
        />
      </figure>
      <div className={styles.facilityDescription}>
        <p className={styles.facilityTitle}>{title}</p>
        <p className={styles.facilityText}>{children}</p>
      </div>
    </div>
  );
}

export default Facility;
