import styles from "./Footer.module.css";
import Navigation from "./Navigation";
import catIcon from "../img/cat-icon.png";

function Footer() {
  return (
    <footer className={"footer " + styles.footer}>
      <div className={styles.footerContainer}>
        <div className={styles.content}>
          <p>©2024 Cat Daycare</p>
          <a className={styles.brand} href="#home">
            <img src={catIcon} alt="Cat Icon" className={styles.icon} />
            <span>Cat DayCare</span>
          </a>
          <a className="btn" href="#pricing">
            Register Now
          </a>
        </div>
        <div className={styles.navigation}>
          <Navigation />
        </div>
      </div>
    </footer>
  );
}

export default Footer;
