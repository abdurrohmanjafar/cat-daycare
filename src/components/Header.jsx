import styles from "./Header.module.css";

function Header() {
  return (
    <header className={"header " + styles.header}>
      <div className={styles.headerContent}>
        <h1 className={styles.headerTitle}>Cat DayCare</h1>
        <p className={styles.headerText}>
          The best cat daycare in Jakarta. Our facilities offer a great way for
          your cat to exercise and play while you're away
        </p>
        <div className={styles.buttonHeader}>
          <a className={"btn " + styles.firstButton} href="#pricing">
            Get Now
          </a>
          <a className={"btn-light " + styles.secondButton} href="#activities">
            Learn More
          </a>
        </div>
      </div>
    </header>
  );
}

export default Header;
