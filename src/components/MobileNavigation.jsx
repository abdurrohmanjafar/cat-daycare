import styles from "./MobileNavigation.module.css";

function MobileNavigation({ children, onClick, showMenu }) {
  function handleClickNav() {
    onClick();
  }
  return (
    <div className={styles.navigation}>
      <ul
        className={`${styles.navigation__item} ${
          showMenu ? styles.show : styles.hidden
        }`}
      >
        <li>{children}</li>
        <li>
          <a
            href="#home"
            onClick={handleClickNav}
            className={"navbar-cta " + styles.mobile}
          >
            Home
          </a>
        </li>
        <li>
          <a
            href="#facilities"
            onClick={handleClickNav}
            className={"navbar-cta " + styles.mobile}
          >
            Facilities
          </a>
        </li>
        <li>
          <a
            href="#pricing"
            onClick={handleClickNav}
            className={"navbar-cta " + styles.mobile}
          >
            Pricing
          </a>
        </li>
        <li>
          <a
            onClick={handleClickNav}
            className={"btn " + styles.button}
            href="#pricing"
          >
            Register Now
          </a>
        </li>
      </ul>
    </div>
  );
}

export default MobileNavigation;
