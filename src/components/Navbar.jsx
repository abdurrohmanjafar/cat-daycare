import { IoClose, IoMenu } from "react-icons/io5";

import styles from "./Navbar.module.css";
import Navigation from "./Navigation";
import MobileNavigation from "./MobileNavigation";
import { useState } from "react";
import { useInView } from "react-intersection-observer";

function Navbar() {
  const [showMenu, setShowMenu] = useState(false);
  const { ref, inView } = useInView({
    threshold: 0,
    initialInView: true,
  });

  function handleToggleMenu() {
    setShowMenu((curr) => !curr);
  }

  return (
    <>
      <div className="navbar" ref={ref} id="home">
        <div className={styles.navbar}>
          <Navigation />
          <div className={styles.navbar__btn}>
            <a className="btn" href="#pricing">
              Register Now
            </a>
          </div>
        </div>
        <div className={styles.mobileNavbar}>
          <div onClick={handleToggleMenu} className={styles.hamburgerOpen}>
            <IoMenu />
          </div>
        </div>
      </div>

      <div
        className={`navbar ${styles.stickyNavbar} ${
          !inView ? styles["stickyNavbar-show"] : styles["stickyNavbar-hidden"]
        }`}
      >
        <div className={styles.navbar}>
          <Navigation />
          <div className={styles.navbar__btn}>
            <a className="btn" href="#pricing">
              Register Now
            </a>
          </div>
        </div>
        <div className={styles.mobileNavbar}>
          <div onClick={handleToggleMenu} className={styles.hamburgerOpen}>
            <IoMenu />
          </div>
        </div>
      </div>

      <div className={styles.mobileNavigation}>
        <MobileNavigation onClick={handleToggleMenu} showMenu={showMenu}>
          <div className={styles.hamburgerClose} onClick={handleToggleMenu}>
            <IoClose />
          </div>
        </MobileNavigation>
      </div>
    </>
  );
}

export default Navbar;
