import styles from "./Navigation.module.css";

function Navigation() {
  return (
    <div className={styles.navigation}>
      <ul className={styles.navigation__item}>
        <li>
          <a href="#home" className="navbar-cta">
            Home
          </a>
        </li>
        <li>
          <a href="#facilities" className="navbar-cta">
            Facilities
          </a>
        </li>
        <li>
          <a href="#pricing" className="navbar-cta">
            Pricing
          </a>
        </li>
      </ul>
    </div>
  );
}

export default Navigation;
