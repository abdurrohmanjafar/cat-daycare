import styles from "./PriceCard.module.css";

function PriceCard({ title, price }) {
  return (
    <div className={styles.card}>
      <p className={styles.cardTitle}>{title}</p>
      <p className={styles.price}>${price}</p>
      <button className="btn">Get</button>
    </div>
  );
}

export default PriceCard;
