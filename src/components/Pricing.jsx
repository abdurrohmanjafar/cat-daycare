import styles from "./Pricing.module.css";
import PriceCard from "./PriceCard";
import { useInView } from "react-intersection-observer";

function Pricing() {
  const { ref, inView } = useInView({
    threshold: 0.15,
    triggerOnce: true,
  });

  return (
    <section
      id="pricing"
      className={`pricing scroll-section ${
        inView ? "section-show" : "section-hidden"
      } ${styles.pricing}`}
      ref={ref}
    >
      <div className={styles.containerPricing}>
        <h2 className={styles.sectionTitle}>Pricing</h2>
        <div className={styles.containerPriceCard}>
          <PriceCard title="Daily Plan" price="3" />
          <PriceCard title="Weekly Plan" price="15" />
          <PriceCard title="Monthly Plan" price="50" />
        </div>
      </div>
    </section>
  );
}

export default Pricing;
